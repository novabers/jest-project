const { sum, multiply, divide } = require('./02-math');

test('adds 1 + 3 to equal 4', () => {
  expect(sum(1, 3)).toBe(4);
});

test('adds 2 * 3 to equal 6', () => {
  expect(multiply(2, 3)).toBe(6);
});

test('adds 2 * 3 to equal 6', () => {
  const op = divide(10, 2);
  expect(op).toBe(5);
});
